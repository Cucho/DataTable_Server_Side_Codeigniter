<?php
	class modelo extends CI_Model {

		public function getP($start, $length, $search, $order, $by) {
			$retornar = array();
			if ($search) {
				$busca = $this->getBuscar($search, $start, $length, $order, $by);
				$retornar['numDataFilter'] = $this->getContarBuscar($search, $start, $length, $order, $by);
				$retornar['data'] = $busca['datos'];
				$retornar['busca'] = "entra en la busqueda";
			}
			else {
				$todo = $this->getTodos($start, $length, $order, $by);
				$retornar['numDataFilter'] = $this->getContar();
				$retornar['data'] = $todo['datos'];
			}

			$retornar['numDataTotal'] = $this->getContar();

			return $retornar;
		}

		function getTodos($start, $length, $order, $by) {
			$this->db->select('*');
			$this->db->join('provincia', 'provincia.provincia_id = comuna.comuna_provincia_id', 'inner');
			$this->db->join('region', 'region.region_id = provincia.provincia_region_id', 'inner');
			if ($by == 0) {
				$this->db->order_by('comuna.comuna_nombre', $order);
			}
			elseif ($by == 1) {
				$this->db->order_by('provincia.provincia_nombre', $order);
			}
			else {
				$this->db->order_by('region.region_nombre', $order);
			}
			$this->db->limit($length, $start);
			$query = $this->db->get('comuna');
			$retornar = array(
				'datos' => $query->result()
			);
			return $retornar;
		}

		function getBuscar($search, $start, $length, $order, $by) {
			$this->db->select('*');
			$this->db->join('provincia', 'provincia.provincia_id = comuna.comuna_provincia_id', 'inner');
			$this->db->join('region', 'region.region_id = provincia.provincia_region_id', 'inner');
			$this->db->like('comuna.comuna_nombre', $search);
			$this->db->or_like('provincia.provincia_nombre', $search);
			$this->db->or_like('region.region_nombre', $search);
			if ($by == 0) {
				$this->db->order_by('comuna.comuna_nombre', $order);
			}
			elseif ($by == 1) {
				$this->db->order_by('provincia.provincia_nombre', $order);
			}
			else {
				$this->db->order_by('region.region_nombre', $order);
			}
			$this->db->limit($length, $start);
			$query = $this->db->get('comuna');
			$retornar = array(
				'datos' => $query->result()
			);
			return $retornar;
		}

		function getContar() {
			$this->db->select('*');
			$this->db->join('provincia', 'provincia.provincia_id = comuna.comuna_provincia_id', 'inner');
			$this->db->join('region', 'region.region_id = provincia.provincia_region_id', 'inner');
			$query = $this->db->get('comuna')->num_rows();
			return $query;
		}

		function getContarBuscar($search, $start, $length, $order, $by) {
			$this->db->select('*');
			$this->db->join('provincia', 'provincia.provincia_id = comuna.comuna_provincia_id', 'inner');
			$this->db->join('region', 'region.region_id = provincia.provincia_region_id', 'inner');
			$this->db->like('comuna.comuna_nombre', $search);
			$this->db->or_like('provincia.provincia_nombre', $search);
			$this->db->or_like('region.region_nombre', $search);
			if ($by == 0) {
				$this->db->order_by('comuna.comuna_nombre', $order);
			}
			elseif ($by == 1) {
				$this->db->order_by('provincia.provincia_nombre', $order);
			}
			else {
				$this->db->order_by('region.region_nombre', $order);
			}
			$quer = $this->db->get('comuna')->num_rows();
			return $quer;
		}
	}
