<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables.css"/>
  <script> var site_url = '<?php echo site_url() ?>'; </script>
  <script> var base_url = '<?php echo base_url() ?>'; </script>
</head>
<body>

<div id="container">
	<?php $this->view('datatable') ?>
</div>

<script src="<?php echo base_url(); ?>assets/jQuery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/datatables.js"></script>
<script>
  $(document).ready(function() {
    $('#tblPersonas').DataTable({
      "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
      'paging': true,
      'info': true,
      'filter': true,
      "ordering": true,
      // 'stateSave': true,
      'processing':true,
      'serverSide':true,
      'language': {
        "url": base_url + "assets/Spanish.json"
      },
      // "order": [[2, "asc"]],
      'ajax': {
        "url": site_url + "/welcome/datatable",
        "type":"POST",
      },
      "columns": [
        { "data": "Comuna" },
        { "data": "Provincia" },
        { "data": "Región" }
      ],
      "columnDefs": [
        {
          "targets": [0],
          "orderable": true,
          "render": function(data, type, row) {
            return "<span><i class='fa fa-check'></i> &nbsp;"+row.COMUNA_NOMBRE+"</span>";
          }
        },
        {
          "targets": [1],
          "orderable": true,
          "render": function(data, type, row) {
            return "<span><i class='fa fa-check'></i> &nbsp;"+row.PROVINCIA_NOMBRE+"</span>";
          }
        },
        {
          "targets": [2],
          "orderable": true,
          "render": function(data, type, row) {
            return "<span><i class='fa fa-check'></i> &nbsp;"+row.REGION_NOMBRE+"</span>";
          }
        }
       ],
    });
  });

	
</script>
</body>
</html>